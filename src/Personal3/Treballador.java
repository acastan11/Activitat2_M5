/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal3;

public abstract class Treballador implements ITreballador {

    int tipus;
    int horesExtres;
    float nomina;
    

    public Treballador(int tipus, int horesExtres, float nomina) {
        this.tipus = tipus;
        this.horesExtres = horesExtres;
        this.nomina = nomina;
    }

    public int getTipusTreballador() {
        return tipus;
    }

    public void setTipus(int tipus) {
        this.tipus = tipus;
    }

    public int getHoresExtres() {
        return horesExtres;
    }

    public void setHoresExtres(int horesExtres) {
        this.horesExtres = horesExtres;
    }

    public float getNomina() {
        return nomina;
    }

    public void setNomina(float nomina) {
        this.nomina = nomina;
    }

    @Override
    public abstract boolean esDirectiu();

    @Override
    public abstract float calcularSou();

}
