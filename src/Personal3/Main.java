
package Personal3;


public class Main {

    public static void main(String[] args) {
        CostPersonal cp = new CostPersonal();
        Treballador t = new TreballadorDirectiu(1, 5, 200);
        Treballador t2 = new TreballadorNormal(3, 5, 200);
       
        Treballador[] treballadors = new Treballador[2];
        treballadors[0] = t;
        treballadors[1] = t2;
        
        System.out.println(t.calcularSou());
        System.out.println(t2.calcularSou());
        System.out.println(cp.CalculaCostDelPersonal(treballadors));

    }

}
