/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal3;

/**
 *
 * @author Alex
 */
public class TreballadorDirectiu extends Treballador {

    public TreballadorDirectiu(int tipus, int horesExtres, float nomina) {
        super(1, horesExtres, nomina);
    }

    @Override
    public boolean esDirectiu() {
        return true;
    }

    @Override
    public float calcularSou() {
        return nomina;
    }

}
