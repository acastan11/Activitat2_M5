/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal3;

/**
 *
 * @author Alex
 */
public class TreballadorNormal extends Treballador {

    public TreballadorNormal(int tipus, int horesExtres, float nomina) {
        super(3, horesExtres, nomina);
    }

    @Override
    public boolean esDirectiu() {
        return false;
    }

    @Override
    public float calcularSou() {
        return nomina + (horesExtres * 20);
    }

}
