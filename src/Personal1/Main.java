package Personal1;

class Main {

    public static void main(String[] args) {
        CostPersonal cp = new CostPersonal();
        Treballador t = new Treballador(1, 7, 200);
        Treballador t2 = new Treballador(3, 5, 200);
        Treballador t3 = new Treballador(3, 5, 500);
        
        Treballador[] treballadors = new Treballador[3];
        treballadors[0] = t;
        treballadors[1] = t2;
        treballadors[2] = t3;

        
        System.out.println(cp.CalculaCostDelPersonal(treballadors));

    }

}
