package Personal1;

public class Treballador {

    int tipus;
    int horesExtres;
    float nomina;
    static public final int DIRECTOR = 1;
    static public final int SUBDIRECTOR = 2;

    public Treballador(int tipus, int horesExtres, float nomina) {
        this.tipus = tipus;
        this.horesExtres = horesExtres;
        this.nomina = nomina;
    }

    public int getTipusTreballador() {
        return tipus;
    }

    public void setTipus(int tipus) {
        this.tipus = tipus;
    }

    public int getHoresExtres() {
        return horesExtres;
    }

    public void setHoresExtres(int horesExtres) {
        this.horesExtres = horesExtres;
    }

    public float getNomina() {
        return nomina;
    }

    public void setNomina(float nomina) {
        this.nomina = nomina;
    }

}
