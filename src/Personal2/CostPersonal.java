package Personal2;

import java.util.ArrayList;
import java.util.Collection;

public class CostPersonal {

    public CostPersonal() {

    }

    /**
     * El metode rep per parametre una array de tipus Treballador, la qual hem
     * generat al main. Comprova el tipus de treballador i en el cas que sigui
     * Director(1) o Subdirector (2), el cost final serà igual a la nomina del
     * treballador. En el cas que el tipus sigui diferent als anterior haurà de
     * sumar les hores extres * 20 a la nomina base. Això ho farà per totes les
     * posicions de l'Array i sumarà les seves quantitats en una variable
     * (costFinal), la qual serà retornada al finalitzar el métode.
     *
     * @param treballadors
     * @return costFinal
     */
    public float CalculaCostDelPersonal(Treballador treballadors[]) {
        float costFinal = 0;
        Treballador treballador = null;
        for (int i = 0; i < treballadors.length; i++) {
            treballador = treballadors[i];
            costFinal = costFinal + calculaSouTreballador(treballador);
        }
        return costFinal;

    }

    public boolean esDirectiu(Treballador treballador) {

        boolean dir = false;
        if (treballador.getTipusTreballador() == Treballador.DIRECTOR || treballador.getTipusTreballador() == Treballador.SUBDIRECTOR) {
            dir = true;
        }
        return dir;
    }

    public float calculaSouTreballador(Treballador treballador) {
        float cost = 0;
        boolean dir = esDirectiu(treballador);

        if (dir) {
            cost = treballador.getNomina();

        } else {
            cost = treballador.getNomina() + (treballador.getHoresExtres() * 20);
        }

        return cost;
    }

}
