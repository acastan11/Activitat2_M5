/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alex
 */
public class CostPersonalTest {

    public CostPersonalTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of CalculaCostDelPersonal method, of class CostPersonal.
     */
    @Test
    public void testCalculaCostDelPersonal() {
        System.out.println("CalculaCostDelPersonal");
        CostPersonal cp = new CostPersonal();
        Treballador t = new Treballador(1, 5, 200);
        Treballador t2 = new Treballador(3, 5, 200);
        Treballador t3 = new Treballador(3, 5, 500);
        Treballador[] treballadors = new Treballador[3];
        Treballador[] treballadors2 = new Treballador[1];

        treballadors[0] = t;
        treballadors[1] = t2;
        treballadors[2] = t3;
        treballadors2[0] = t;

        float expResult = 1100;
        float result = cp.CalculaCostDelPersonal(treballadors);
        assertEquals(expResult, result, 0.0);
        expResult = 200;
        result = cp.CalculaCostDelPersonal(treballadors2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of esDirectiu method, of class CostPersonal.
     */
    @Test
    public void testEsDirectiu() {
        System.out.println("esDirectiu");
        CostPersonal cp = new CostPersonal();
        Treballador t = new Treballador(1, 5, 200);
        Treballador t2 = new Treballador(3, 5, 200);

        boolean result = cp.esDirectiu(t);
        boolean expResult = true;
        assertEquals(expResult, result);
        result = cp.esDirectiu(t2);
        expResult = false;
        assertEquals(expResult, result);

    }

    /**
     * Test of calculaSouTreballador method, of class CostPersonal.
     */
    @Test
    public void testCalculaSouTreballador() {
        System.out.println("calculaSouTreballador");
        CostPersonal cp = new CostPersonal();
        Treballador t = new Treballador(1, 5, 200);
        Treballador t2 = new Treballador(3, 5, 200);

        float expResult = 200;
        float result = cp.calculaSouTreballador(t);
        assertEquals(expResult, result, 0.0);
        
        expResult = 300;
        result = cp.calculaSouTreballador(t2);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.

    }

}
